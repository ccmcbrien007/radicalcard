import 'package:test/test.dart';
import 'package:radicalcard/model/contact_card.dart';
import 'package:radicalcard/constants.dart';
import 'package:radicalcard/utils/code_utils.dart';

void main() {
  const name = 'Chris McBrien';
  const title = 'Space Cowboy';
  const phone = '403-555-0000';
  const email = 'chris.mcbrien@nowhere.email';

  const expectedEncodedInfo = name +
      kInfoEncodingDelimeter +
      title +
      kInfoEncodingDelimeter +
      phone +
      kInfoEncodingDelimeter +
      email;

  group('qr code', () {
    test('Contact info should encode', () async {
      ContactCard info = ContactCard(name, title, phone, email);
      String encodedInfo = CodeUtils.encodeInfo(info);

      expect(expectedEncodedInfo, equals(encodedInfo));
    });

    test('Contact info should decode', () async {
      ContactCard expected = ContactCard(name, title, phone, email);
      ContactCard info = CodeUtils.decodeContactInfo(expectedEncodedInfo);

      expect(expected, equals(info));
    });
  });
}
