import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:shared_preferences_settings/shared_preferences_settings.dart';
import 'package:radicalcard/constants.dart';
import 'package:radicalcard/utils/settings_utils.dart';

class MockSettings extends Mock implements Settings {}

void main() {
  group('settings', () {
    test('Instance should be shared', () async {
      final SettingsUtils iut = await getMockedInstance();

      Settings a = iut.getSettings();
      Settings b = iut.getSettings();

      expect(a, equals(b));
    });
  });

  group('boolean setters', () {
    test('IsSetup should mutate', () async {
      final SettingsUtils iut = await getMockedInstance();
      final MockSettings settingsMock = iut.getSettings();

      iut.setIsSetup();

      verify(settingsMock.save(kPrefIsSetup, true)).called(1);
    });

    test('IsIntroShown should mutate', () async {
      final SettingsUtils iut = await getMockedInstance();
      final MockSettings settingsMock = iut.getSettings();

      iut.setIntroShown();

      verify(settingsMock.save(kPrefIntroShown, true)).called(1);
    });
  });

  group('string setters', () {
    test('imageFile should mutate', () async {
      final SettingsUtils iut = await getMockedInstance();
      final MockSettings settingsMock = iut.getSettings();
      final String value = 'newPath';

      iut.setImageFile(value);

      verify(settingsMock.save(kPrefKeyImageFile, value)).called(1);
    });
  });
}

Future<SettingsUtils> getMockedInstance() async {
  final SettingsUtils iut = SettingsUtils();
  final MockSettings settingsMock = MockSettings();
  iut.setSettings(settingsMock);

  when(settingsMock.getString(any, any))
      .thenAnswer((realInvocation) => Future.value('value'));
  when(settingsMock.getBool(any, any))
      .thenAnswer((realInvocation) => Future.value(true));

  return iut;
}
