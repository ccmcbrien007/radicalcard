import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences_settings/shared_preferences_settings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:radicalcard/components/info_area.dart';
import 'package:radicalcard/constants.dart';

void main() {
  testWidgets('InfoArea displays initial values', (WidgetTester tester) async {
    const String initialName = 'Jean-Luc Picard';
    const String initialTitle = 'Starship Captain';
    const String initialPhone = '111-222-3333';
    const String initialEmail = 'jlpicard@starfleet.gov';

    SharedPreferences.setMockInitialValues({
      kPrefKeyName: initialName,
      kPrefKeyTitle: initialTitle,
      kPrefKeyPhone: initialPhone,
      kPrefKeyEmail: initialEmail,
    });

    await tester.pumpWidget(
      MaterialApp(
        home: ConstrainedBox(
          child: InfoArea(),
          constraints: BoxConstraints(maxWidth: 240.0),
        ),
      ),
    );
    await tester.pumpAndSettle(Duration(milliseconds: 250));

    Finder nameFieldFinder = find.byKey(Key(kPrefKeyName));
    Finder titleFieldFinder = find.byKey(Key(kPrefKeyTitle));
    Finder phoneFieldFinder = find.byKey(Key(kPrefKeyPhone));
    Finder emailFieldFinder = find.byKey(Key(kPrefKeyEmail));

    expect(nameFieldFinder, findsOneWidget);
    expect(titleFieldFinder, findsOneWidget);
    expect(phoneFieldFinder, findsOneWidget);
    expect(emailFieldFinder, findsOneWidget);

    expect(find.text(initialName), findsOneWidget);
    expect(find.text(initialTitle), findsOneWidget);
    expect(find.text(initialEmail), findsOneWidget);
    expect(find.text(initialPhone), findsOneWidget);
  });

  testWidgets('InfoArea set new values', (WidgetTester tester) async {
    const String newName = 'James T. Kirk';
    const String newTitle = 'Admiral';
    const String newEmail = 'jtkirk@starfleet.gov';
    const String newPhone = '011000111217';

    SharedPreferences.setMockInitialValues({});

    await tester.pumpWidget(
      MaterialApp(
        home: InfoArea(),
      ),
    );

    Finder nameFieldFinder = find.byKey(Key(kPrefKeyName));
    Finder titleFieldFinder = find.byKey(Key(kPrefKeyTitle));
    Finder phoneFieldFinder = find.byKey(Key(kPrefKeyPhone));
    Finder emailFieldFinder = find.byKey(Key(kPrefKeyEmail));

    expect(nameFieldFinder, findsOneWidget);
    expect(titleFieldFinder, findsOneWidget);
    expect(phoneFieldFinder, findsOneWidget);
    expect(emailFieldFinder, findsOneWidget);

    Settings().save(kPrefKeyName, newName);
    Settings().save(kPrefKeyTitle, newTitle);
    Settings().save(kPrefKeyEmail, newEmail);
    Settings().save(kPrefKeyPhone, newPhone);
    await tester.pumpAndSettle(Duration(milliseconds: 250));

    Finder newNameFinder = find.text(newName);
    Finder newTitleFinder = find.text(newName);
    Finder newPhoneFinder = find.text(newName);
    Finder newEmailFinder = find.text(newName);

    expect(newNameFinder, findsOneWidget,
        reason: 'Unable to find field with new name = $newName');
    expect(newTitleFinder, findsOneWidget,
        reason: 'Unable to find field with new title = $newTitle');
    expect(newPhoneFinder, findsOneWidget,
        reason: 'Unable to find field with new phone = $newPhone');
    expect(newEmailFinder, findsOneWidget,
        reason: 'Unable to find field with new email = $newEmail');
  });
}
