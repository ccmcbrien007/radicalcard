import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:radicalcard/components/info_card.dart';
import 'package:shared_preferences_settings/shared_preferences_settings.dart';

void main() {
  const kPrefKeyName = 'KEY';
  final String defaultText = 'test default';

  testWidgets('InfoCard smoke test', (WidgetTester tester) async {
    SharedPreferences.setMockInitialValues({
      kPrefKeyName: defaultText,
    });

    // Build our app and trigger a frame.
    final IconData icon = Icons.phone;

    await tester.pumpWidget(
      MaterialApp(
        home: InfoCard(icon, kPrefKeyName),
      ),
    );
    await tester.pumpAndSettle(Duration(milliseconds: 250));

    Finder fieldFinder = find.byKey(Key(kPrefKeyName));

    // Verify that our counter starts at 0.
    expect(fieldFinder, findsOneWidget,
        reason: 'No field with key=$kPrefKeyName');
    expect(find.text(defaultText), findsOneWidget,
        reason: 'No field with text=$defaultText');
    expect(find.byIcon(icon), findsOneWidget,
        reason: 'No field with expected icon');

    final String newValue = 'New value';
    Settings().save(kPrefKeyName, newValue);
    await tester.pumpAndSettle(Duration(milliseconds: 250));

    expect(fieldFinder, findsOneWidget);
    expect(find.text(defaultText), findsNothing);
    expect(find.text(newValue), findsOneWidget);
  });
}
