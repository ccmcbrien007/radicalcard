const kApplicationName = 'RadicalCard';
const kPrefIsSetup = 'PREF_KEY_ISSETUP';
const kPrefIntroShown = 'PREF_KEY_INTROSHOWN';
const kPrefKeyEmail = 'PREF_KEY_EMAIL';
const kPrefKeyPhone = 'PREF_KEY_PHONE';
const kPrefKeyName = 'PREF_KEY_NAME';
const kPrefKeyTitle = 'PREF_KEY_TITLE';
const kPrefKeyImageFile = 'PREF_KEY_IMAGE_FILE';
const kInfoEncodingDelimeter = '|';
const String kDatabaseName = 'contacts.db';
const String kTableNameContactCard = 'Card';
const String kColumnNameId = 'id';
const String kColumnNameName = 'name';
const String kColumnNameContactCardLat = 'latitude';
const String kColumnNameContactCardLon = 'longitude';
// Todo Qualify navbar index names
const int kIndexEdit = 0;
const int kIndexDelete = 1;
const int kIndexMap = 2;
