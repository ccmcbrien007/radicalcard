import 'package:flutter/material.dart';
import 'package:about/about.dart';
import 'package:url_launcher/url_launcher.dart';

class AppAboutPage extends StatelessWidget {
  static final String id = 'app_about_page';

  @override
  Widget build(BuildContext context) {
    return AboutPage(
      title: Text('About RadicalCard'),
      applicationVersion: 'Version {{ version }}, build #{{ buildNumber }}',
      applicationDescription: Text(
        'Your electronic contact card.  Save a tree, socially distance, and reduce your carbon footprint.  Share your vital details and scan others, quickly.  Inspired by Angela Yu at London App Brewery.',
        textAlign: TextAlign.justify,
      ),
      applicationIcon: Image(image: AssetImage('images/FeatureGraphic.png')),
      applicationLegalese: '© Chris McBrien , {{ year }}',
      children: <Widget>[
        MarkdownPageListTile(
          filename: 'LICENSE.md',
          title: InkWell(
            child: Text('View License'),
            onTap: () => _launchURL(),
          ),
          icon: Icon(Icons.description),
        ),
      ],
    );
  }

  _launchURL() async {
    const url = 'https://gitlab.com/ccmcbrien007/radicalcard/-/wikis/License';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
