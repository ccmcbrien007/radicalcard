import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:radicalcard/pages/intro_page.dart';
import 'package:radicalcard/pages/settings_page.dart';
import 'package:radicalcard/utils/settings_utils.dart';

import 'info_page.dart';

class LoadingPage extends StatelessWidget {
  static final String id = 'loading_screen';

  @override
  Widget build(BuildContext context) {
    final SettingsUtils _settings = SettingsUtils();

    _settings.isIntroShown().then((isIntroShown) {
      if (!isIntroShown) {
        Navigator.pushNamed(context, IntroPage.id).then((value) {
          Navigator.pushNamed(context, SettingsPage.id).then((value) =>
              Navigator.pushNamedAndRemoveUntil(
                  context, InfoPage.id, (route) => false));
        });
      } else {
        _settings.isSetup().then((isSetup) {
          if (!isSetup) {
            Navigator.pushNamed(context, SettingsPage.id).then((value) =>
                Navigator.pushNamedAndRemoveUntil(
                    context, InfoPage.id, (route) => false));
          } else {
            Navigator.pushNamedAndRemoveUntil(
                context, InfoPage.id, (route) => false);
          }
        });
      }
    });

    return Scaffold(
      body: Center(
        child: SpinKitDoubleBounce(
          color: Colors.white,
          size: 100.0,
        ),
      ),
    );
  }
}
