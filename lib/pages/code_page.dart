import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:location/location.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:radicalcard/model/contact_card.dart' as radical;
import 'package:radicalcard/pages/contact_page.dart';
import 'package:radicalcard/utils/code_utils.dart';
import 'package:radicalcard/utils/data_provider.dart';
import 'package:radicalcard/utils/location_provider.dart';
import 'package:radicalcard/utils/settings_utils.dart';
import 'package:radicalcard/utils/ui_utilities.dart';

class CodePage extends StatelessWidget {
  static String id = 'code_page';
  final int camera = -1;

  @override
  Widget build(BuildContext context) {
    final Future<String> _encodedInfoGetter =
        Future<String>.delayed(Duration(milliseconds: 50), () async {
      radical.ContactCard card = await SettingsUtils().getMyInfo();
      return CodeUtils.encodeInfo(card);
    });

    return FutureBuilder<String>(
        future: _encodedInfoGetter,
        builder: (BuildContext innerContext, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasData) {
            return Scaffold(
              appBar: AppBar(
                title: Text('Scan Me'),
              ),
              body: QrImage(data: snapshot.data),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  _scan(context);
                },
                child: Icon(Icons.add_a_photo),
              ),
              backgroundColor: Colors.white,
            );
          } else {
            return Container();
          }
        });
  }

  Future<void> _scan(BuildContext context) async {
    ScanResult codeScanner = await BarcodeScanner.scan(
      options: ScanOptions(
        useCamera: camera,
        autoEnableFlash: false,
      ),
    );
    if (codeScanner.type != ResultType.Cancelled) {
      final _scannedInfo = codeScanner.rawContent;
      try {
        final radical.ContactCard _contactInfo =
            CodeUtils.decodeContactInfo(_scannedInfo);
        LocationData location = await LocationProvider().getLocation();
        if (location != null) {
          _contactInfo.latitude = location.latitude;
          _contactInfo.longitude = location.longitude;
        }
        int contactId =
            await DataProvider.provider.addCardToDatabase(_contactInfo);
        _contactInfo.id = contactId;
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => ContactPage(contactId)));
      } catch (e) {
        String message = e.toString();
        print('ERROR: ' + message);
        await DialogUtility().showMyDialog(
            context, 'There was an error', message, ['OK'], (int) => {});
      }
    }
  }
}
