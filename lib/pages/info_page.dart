import 'package:flutter/material.dart';
import 'package:radicalcard/components/info_area.dart';
import 'package:radicalcard/constants.dart';
import 'package:radicalcard/pages/about_page.dart';
import 'package:radicalcard/pages/settings_page.dart';

import 'code_page.dart';
import 'contact_list_page.dart';

class InfoPage extends StatelessWidget {
  static final String id = 'info_screen';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Image(
                image: AssetImage('images/FeatureGraphic.png'),
              ),
              decoration: BoxDecoration(
                color: Colors.black,
              ),
            ),
            ListTile(
              leading: Icon(Icons.contacts),
              title: Text('Contact List'),
              onTap: () {
                Navigator.pushNamed(context, ContactListPage.id)
                    .then((value) => Navigator.pop(context));
              },
            ),
            ListTile(
              leading: Icon(Icons.settings),
              title: Text('Settings'),
              onTap: () {
                Navigator.pushNamed(context, SettingsPage.id)
                    .then((value) => Navigator.pop(context));
              },
            ),
            ListTile(
              leading: Icon(Icons.help),
              title: Text('About'),
              onTap: () {
                Navigator.pushNamed(context, AppAboutPage.id);
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        title: const Text(kApplicationName),
      ),
      /*backgroundColor: Colors.teal,*/
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: InfoArea(),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(
            context,
            CodePage.id,
          );
        },
        child: Icon(Icons.qr_code),
      ),
    );
  }
}
