import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:radicalcard/model/contact_card.dart';
import 'package:radicalcard/pages/contact_edit_age.dart';
import 'package:radicalcard/utils/data_provider.dart';
import 'package:radicalcard/utils/ui_utilities.dart';
import 'package:maps_launcher/maps_launcher.dart';

import '../constants.dart';
import '../style_constants.dart';

class ContactPage extends StatefulWidget {
  static String id = 'contact_page';
  final int _contactId;

  ContactPage(this._contactId);

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  int _navBarIndex = 0;
  ContactCard info;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Contact'),
      ),
      body: FutureBuilder(
        future: DataProvider.provider.getPersonWithId(widget._contactId),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            info = snapshot.data;
            return _buildPage(info);
          } else {
            return Container();
          }
        },
      ),
      bottomNavigationBar: _getNavigationBar(context),
    );
  }

  void _handleNavBarTapped(BuildContext context, int index, int contactId) {
    setState(() {
      _navBarIndex = index;
    });
    switch (index) {
      case kIndexEdit:
        {
          _handleEditAction(contactId);
        }
        break;
      case kIndexDelete:
        {
          _handleDeleteAction(contactId);
        }
        break;
      case kIndexMap:
        {
          _handleMapAction(context, contactId);
        }
        break;
    }
  }

  Widget _buildPage(ContactCard info) {
    return Column(
      children: <Widget>[
        Text(info.name, style: kNameTextStyle),
        Text(info.title, style: kTitleTextStyle),
        SizedBox(
            height: 20.0,
            width: 150.0,
            child: Divider(color: Colors.teal.shade100)),
        Card(
          color: Colors.white,
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
          child: ListTile(
            leading: Icon(Icons.phone, color: Colors.black),
            title: Text(info.phone, style: kBoxTextStyle),
          ),
        ),
        Card(
          color: Colors.white,
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
          child: ListTile(
            leading: Icon(Icons.email, color: Colors.black),
            title: Text(info.email, style: kBoxTextStyle),
          ),
        ),
        Card(
          color: Colors.white,
          margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
          child: ListTile(
            leading: Icon(Icons.note, color: Colors.black),
            title: Text(info.notes != null ? info.notes : '',
                style: kBoxTextStyle),
          ),
        ),
      ],
    );
  }

  Widget _getNavigationBar(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: _navBarIndex,
      onTap: (int index) async {
        _handleNavBarTapped(context, index, widget._contactId);
      },
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.edit),
            backgroundColor: Colors.blue,
            label: 'Edit'),
        BottomNavigationBarItem(
          icon: Icon(Icons.delete),
          backgroundColor: Colors.blue,
          label: 'Delete',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.map),
          backgroundColor: Colors.blue,
          label: 'Map Location',
        ),
      ],
    );
  }

  void _handleMapAction(BuildContext context, int contactId) async {
    DataProvider.provider.getPersonWithId(contactId).then((contact) {
      if (contact.hasLocation()) {
        MapsLauncher.launchCoordinates(contact.latitude, contact.longitude,
            '${contact.name} scanned here');
      } else {
        DialogUtility().showMyDialog(
            context,
            'Map Location',
            'There is no location recorded for ${contact.name}',
            ['OK'],
            (value) {});
      }
    });
  }

  void _handleDeleteAction(int id) {
    DialogUtility().showMyDialog(
        context,
        'Confirm delete',
        'Are you sure you want to delete this contact',
        ['Yes', 'No'], (selectedButton) async {
      print('Dialog dismissed with index=$selectedButton');
      if (selectedButton == 'Yes') {
        print('Yes selected.  Deleting contact.');
        await DataProvider.provider.deleteCardWithId(id);
        Navigator.pop(context);
      }
    });
  }

  void _handleEditAction(int contactId) {
    Navigator.push(context,
            MaterialPageRoute(builder: (context) => ContactEditPage(contactId)))
        .then((value) async {
      ContactCard updatedCard =
          await DataProvider.provider.getPersonWithId(widget._contactId);
      setState(() {
        info = updatedCard;
      });
    });
  }
}
