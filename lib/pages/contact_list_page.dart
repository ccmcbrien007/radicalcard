import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:radicalcard/model/contact_card.dart';
import 'package:radicalcard/pages/contact_page.dart';
import 'package:radicalcard/utils/data_provider.dart';

import '../style_constants.dart';

class ContactListPage extends StatefulWidget {
  static String id = 'contact_list_page';

  @override
  _ContactListPageState createState() => _ContactListPageState();
}

class _ContactListPageState extends State<ContactListPage> {
  List<ContactCard> _contactList;

  @override
  void initState() {
    super.initState();
    _refreshList();
  }

  void _refreshList() {
    DataProvider.provider.getAllCards().then((value) {
      setState(() {
        _contactList = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: Text('Contacts List'),
      ),
      body: _contactList != null
          ? _getContactListWidget(_contactList)
          : SpinKitDoubleBounce(
              color: Colors.white,
              size: 100.0,
            ),
    );
  }

  Widget _getContactListWidget(List<ContactCard> data) {
    if (data.length > 0) {
      return ListView.builder(
        padding: EdgeInsets.all(8),
        itemCount: data.length,
        itemBuilder: (BuildContext buildContext, int index) {
          return Card(
            child: FlatButton(
              padding: EdgeInsets.all(10),
              onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ContactPage(data[index].id)))
                  .then((value) => _refreshList()),
              child: Column(
                children: [
                  Text(
                    data[index].name,
                    style: kNameTextStyle.copyWith(fontSize: 22),
                  ),
                  Text(
                    data[index].title,
                    style: kTitleTextStyle.copyWith(fontSize: 18),
                  ),
                ],
              ),
            ),
          );
        },
      );
    } else {
      return Center(
          child: Column(
        children: [
          Text(
            'No entries',
            style: kNameTextStyle,
          ),
          Text('Meet some people and get scanning...'),
        ],
      ));
    }
  }
}
