import 'dart:io';
import 'package:flutter/material.dart';
import 'package:shared_preferences_settings/shared_preferences_settings.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/foundation.dart';

import '../constants.dart';
import '../utils/settings_utils.dart';

class SettingsPage extends StatefulWidget {
  static String id = 'settings_page';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  final SettingsUtils _settingsUtils = SettingsUtils();
  PickedFile _imageFile;
  dynamic _pickImageError;
  bool isVideo = false;
  String _retrieveDataError;

  final ImagePicker _picker = ImagePicker();
  final TextEditingController maxWidthController = TextEditingController();
  final TextEditingController maxHeightController = TextEditingController();
  final TextEditingController qualityController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _settingsUtils.setIsSetup();
  }

  @override
  void dispose() {
    maxWidthController.dispose();
    maxHeightController.dispose();
    qualityController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SettingsScreen(
      title: "Application Settings",
      children: [
        TextFieldModalSettingsTile(
          settingKey: kPrefKeyName,
          title: 'Type your name',
          defaultValue: '',
          cancelCaption: 'Cancel',
          okCaption: 'Save Name',
        ),
        TextFieldModalSettingsTile(
          settingKey: kPrefKeyTitle,
          title: 'Type your title',
          defaultValue: '',
          cancelCaption: 'Cancel',
          okCaption: 'Save Title',
        ),
        TextFieldModalSettingsTile(
          settingKey: kPrefKeyPhone,
          title: 'Type your phone',
          defaultValue: '',
          cancelCaption: 'Cancel',
          okCaption: 'Save Phone',
          keyboardType: TextInputType.phone,
        ),
        TextFieldModalSettingsTile(
          settingKey: kPrefKeyEmail,
          title: 'Type your email',
          defaultValue: '',
          cancelCaption: 'Cancel',
          okCaption: 'Save Email',
          keyboardType: TextInputType.emailAddress,
        ),
        SettingsContainer(
          child: Text('Profile Image'),
        ),
        !kIsWeb && defaultTargetPlatform == TargetPlatform.android
            ? FutureBuilder<void>(
                future: retrieveLostData(),
                builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.none:
                    case ConnectionState.waiting:
                      return const Text(
                        'Loading image',
                        textAlign: TextAlign.center,
                      );
                    case ConnectionState.done:
                      return _previewImage();
                    default:
                      if (snapshot.hasError) {
                        return Text(
                          'Pick image/video error: ${snapshot.error}}',
                          textAlign: TextAlign.center,
                        );
                      } else {
                        return const Text(
                          'You have not yet picked an image.',
                          textAlign: TextAlign.center,
                        );
                      }
                  }
                },
              )
            : _previewImage(),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: FloatingActionButton(
            onPressed: () {
              isVideo = false;
              _onImageButtonPressed(ImageSource.gallery, context: context);
            },
            heroTag: 'image0',
            tooltip: 'Pick Image from gallery',
            child: const Icon(Icons.photo_library),
          ),
        ),
      ],
    );
  }

  Future<void> retrieveLostData() async {
    final LostData response = await _picker.getLostData();
    if (response.isEmpty) {
      String _initialSelectedImage = await SettingsUtils().getImageFile();
      if (_initialSelectedImage.length > 0) {
        var _pickedFile = PickedFile(_initialSelectedImage);
        _imageFile = _pickedFile;
      } else {
        _imageFile = null;
      }
    } else {
      if (response.file != null) {
        if (response.type == RetrieveType.video) {
          isVideo = true;
        } else {
          isVideo = false;
          _imageFile = response.file;
        }
      } else {
        _retrieveDataError = response.exception.code;
      }
    }
  }

  Widget _previewImage() {
    final Text retrieveError = _getRetrieveErrorWidget();
    if (retrieveError != null) {
      return retrieveError;
    }
    if (_imageFile != null) {
      if (kIsWeb) {
        // Why network?
        // See https://pub.dev/packages/image_picker#getting-ready-for-the-web-platform
        return Image.network(_imageFile.path);
      } else {
        return Center(
          child: CircleAvatar(
            backgroundImage: Image.file(File(_imageFile.path)).image,
            radius: 50.0,
          ),
        );
      }
    } else if (_pickImageError != null) {
      return Text(
        'Pick image error: $_pickImageError',
        textAlign: TextAlign.center,
      );
    } else {
      return const Text(
        'You have not yet picked an image.',
        textAlign: TextAlign.center,
      );
    }
  }

  Text _getRetrieveErrorWidget() {
    if (_retrieveDataError != null) {
      final Text result = Text(_retrieveDataError);
      _retrieveDataError = null;
      return result;
    }
    return null;
  }

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    try {
      final pickedFile = await _picker.getImage(
        source: source,
      );
      _settingsUtils.setImageFile(pickedFile.path);
      setState(() {
        _imageFile = pickedFile;
      });
    } catch (e) {
      setState(() {
        _pickImageError = e;
      });
    }
  }
}

typedef void OnPickImageCallback(
    double maxWidth, double maxHeight, int quality);
