import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:radicalcard/model/contact_card.dart';
import 'package:radicalcard/utils/data_provider.dart';
import 'package:radicalcard/utils/ui_utilities.dart';

import '../style_constants.dart';

class ContactEditPage extends StatefulWidget {
  static String id = 'contact_edit_page';

  final int _contactId;

  ContactEditPage(this._contactId);

  @override
  _ContactEditPageState createState() => _ContactEditPageState();
}

class _ContactEditPageState extends State<ContactEditPage> {
  final _formKey = GlobalKey<FormState>();
  Form form;
  ContactCard contact;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit contact'),
        actions: <Widget>[
          FlatButton(
              onPressed: () async {
                if (form != null && _formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  await DataProvider.provider.updatePerson(contact);
                  DialogUtility().showMyDialog(
                      context, '', 'Contact card saved', ['OK'], (value) {});
                }
              },
              child: Text(
                'Save',
                style: kTextStyleAppBarAction,
              ))
        ],
      ),
      body: FutureBuilder(
        future: DataProvider.provider.getPersonWithId(widget._contactId),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Container();
          } else {
            this.contact = snapshot.data;
            form = _getForm(contact);
            return form;
          }
        },
      ),
    );
  }

  Widget _getForm(ContactCard contact) {
    Form result = Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Text(
            'Name',
            style: kTextStyleEditFieldLabel,
          ),
          TextFormField(
            onSaved: (newValue) => contact.name = newValue,
            decoration: const InputDecoration(hintText: 'Enter contact name'),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter a name';
              } else {
                return null;
              }
            },
            initialValue: contact.name,
          ),
          SizedBox(
            height: 40,
          ),
          Text('Title', style: kTextStyleEditFieldLabel),
          TextFormField(
            onSaved: (newValue) => contact.title = newValue,
            decoration: const InputDecoration(hintText: 'Enter contact title'),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter a title';
              } else {
                return null;
              }
            },
            initialValue: contact.title,
          ),
          SizedBox(
            height: 40,
          ),
          Text('Phone Number', style: kTextStyleEditFieldLabel),
          TextFormField(
            onSaved: (newValue) => contact.phone = newValue,
            decoration:
                const InputDecoration(hintText: 'Enter contact phone number'),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter a phone number';
              } else {
                return null;
              }
            },
            initialValue: contact.phone,
          ),
          SizedBox(
            height: 40,
          ),
          Text('Email', style: kTextStyleEditFieldLabel),
          TextFormField(
            onSaved: (newValue) => contact.email = newValue,
            decoration: const InputDecoration(hintText: 'Enter contact email'),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter a email';
              } else {
                return null;
              }
            },
            initialValue: contact.email,
          ),
          SizedBox(
            height: 40,
          ),
          Text('Notes', style: kTextStyleEditFieldLabel),
          TextFormField(
            onSaved: (newValue) => contact.notes = newValue,
            decoration: const InputDecoration(hintText: 'Enter notes'),
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter notes';
              } else {
                return null;
              }
            },
            initialValue: contact.notes,
          ),
        ],
      ),
    );
    return result;
  }
}
