import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:radicalcard/pages/about_page.dart';
import 'package:radicalcard/pages/code_page.dart';
import 'package:radicalcard/pages/contact_list_page.dart';
import 'package:radicalcard/pages/info_page.dart';
import 'package:radicalcard/pages/intro_page.dart';
import 'package:radicalcard/pages/loading_page.dart';
import 'package:radicalcard/pages/settings_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle.dark.copyWith(statusBarColor: Colors.transparent),
    );

    return MaterialApp(
        debugShowCheckedModeBanner: false,
        darkTheme: ThemeData.dark(),
        themeMode: ThemeMode.dark,
        initialRoute: LoadingPage.id,
        routes: {
          LoadingPage.id: (context) => LoadingPage(),
          IntroPage.id: (context) => IntroPage(),
          SettingsPage.id: (context) => SettingsPage(),
          AppAboutPage.id: (context) => AppAboutPage(),
          InfoPage.id: (context) => InfoPage(),
          CodePage.id: (context) => CodePage(),
          ContactListPage.id: (context) => ContactListPage(),
        });
  }
}
