import 'dart:io';

import 'package:flutter/material.dart';
import 'package:shared_preferences_settings/shared_preferences_settings.dart';
import 'package:radicalcard/constants.dart';
import 'package:radicalcard/utils/settings_utils.dart';

class PrefSensitiveProfileImage extends StatelessWidget {
  final Settings _settings = SettingsUtils().getSettings();
  final Future<String> _imageFileGetter = Future<String>.delayed(
      Duration(milliseconds: 250), () => SettingsUtils().getImageFile());

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<String>(
      future: _imageFileGetter,
      builder: (BuildContext innerContext, AsyncSnapshot<String> snapshot) {
        Widget result;

        if (snapshot.hasData) {
          String _imageFile = snapshot.data;
          if (_imageFile.length > 0) {
            result = _settings.onStringChanged(
              settingKey: kPrefKeyImageFile,
              defaultValue: _imageFile,
              childBuilder: (BuildContext context, String value) {
                if (value != null) {
                  return CircleAvatar(
                    backgroundImage: Image.file(File(value)).image,
                    radius: 50.0,
                  );
                } else {
                  return Container();
                }
              },
            );
          } else {
            result = Container();
          }
        } else {
          result = Container();
        }
        return result;
      },
    );
  }
}
