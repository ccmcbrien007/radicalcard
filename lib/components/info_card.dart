import 'package:flutter/material.dart';
import 'package:radicalcard/style_constants.dart';
import 'package:radicalcard/utils/settings_utils.dart';

class InfoCard extends StatelessWidget {
  final IconData _icon;
  final String _prefKey;

  InfoCard(this._icon, this._prefKey);

  @override
  Widget build(BuildContext context) {
    final Future<String> _initialValueGetter = Future<String>.delayed(
        Duration(milliseconds: 50),
        () => SettingsUtils().getSettings().getString(_prefKey, ''));

    return FutureBuilder<String>(
        future: _initialValueGetter,
        builder: (BuildContext innerContext, AsyncSnapshot<String> snapshot) {
          return _getFuture(snapshot);
        });
  }

  Widget _getFuture(AsyncSnapshot<String> snapshot) {
    Widget result = _getDefaultWidget();

    if (snapshot.hasData) {
      String _initialValue = snapshot.data;

      result = Card(
        color: Colors.white,
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
        child: ListTile(
          leading: Icon(_icon, color: Colors.black),
          title: SettingsUtils().getSettings().onStringChanged(
                settingKey: _prefKey,
                defaultValue: _initialValue,
                childBuilder: (BuildContext context, String value) {
                  return Text(value, key: Key(_prefKey), style: kBoxTextStyle);
                },
              ),
        ),
      );
    }
    return result;
  }

  Widget _getDefaultWidget() {
    return Card(
        color: Colors.white,
        margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
        child: ListTile(
            leading: Icon(_icon, color: Colors.black),
            title: Text(
              '',
              key: Key(_prefKey),
            )));
  }
}
