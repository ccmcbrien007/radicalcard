import 'package:flutter/material.dart';
import 'package:radicalcard/components/PrefSensitiveProfileImage.dart';
import 'package:radicalcard/components/info_text.dart';
import 'package:radicalcard/constants.dart';
import 'package:radicalcard/style_constants.dart';
import 'info_card.dart';

class InfoArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        PrefSensitiveProfileImage(),
        InfoText(kPrefKeyName, kNameTextStyle),
        InfoText(kPrefKeyTitle, kTitleTextStyle),
        SizedBox(
            height: 20.0,
            width: 150.0,
            child: Divider(color: Colors.teal.shade100)),
        InfoCard(Icons.phone, kPrefKeyPhone),
        InfoCard(Icons.email, kPrefKeyEmail),
      ],
    );
  }
}
