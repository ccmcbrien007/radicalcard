import 'package:flutter/widgets.dart';
import 'package:radicalcard/utils/settings_utils.dart';

class InfoText extends StatelessWidget {
  final String _prefKey;
  final TextStyle _textStyle;
  InfoText(this._prefKey, this._textStyle);

  @override
  Widget build(BuildContext context) {
    final Future<String> _initialValueGetter = Future<String>.delayed(
        Duration(milliseconds: 250),
        () => SettingsUtils().getSettings().getString(_prefKey, ''));

    return FutureBuilder<String>(
        future: _initialValueGetter,
        builder: (BuildContext innerContext, AsyncSnapshot<String> snapshot) {
          Widget result = Text(
            '',
            key: Key(_prefKey),
            style: _textStyle,
          );
          if (snapshot.hasData) {
            String _initialValue = snapshot.data;
            result = SettingsUtils().getSettings().onStringChanged(
                settingKey: _prefKey,
                defaultValue: _initialValue,
                childBuilder: (BuildContext innerContext, String value) {
                  return Text(
                    value,
                    key: Key(_prefKey),
                    style: _textStyle,
                  );
                });
          }
          return result;
        });
  }
}
