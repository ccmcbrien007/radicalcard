import 'package:radicalcard/constants.dart';
import 'package:radicalcard/model/contact_card.dart';

class CodeUtils {
  static ContactCard decodeContactInfo(String scannedInfo) {
    var _fields = scannedInfo.split(kInfoEncodingDelimeter);
    if (_fields.length == 4) {
      String name = _fields[0];
      String title = _fields[1];
      String phone = _fields[2];
      String email = _fields[3];
      ContactCard info = ContactCard(name, title, phone, email);
      return info;
    } else {
      throw Exception('Invalid code was scanned');
    }
  }

  static String encodeInfo(ContactCard info) {
    return '${info.name}' +
        '$kInfoEncodingDelimeter' +
        '${info.title}' +
        '$kInfoEncodingDelimeter' +
        '${info.phone}' +
        '$kInfoEncodingDelimeter' +
        '${info.email}';
  }
}
