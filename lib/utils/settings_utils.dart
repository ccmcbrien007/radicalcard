import 'package:radicalcard/model/contact_card.dart';
import 'package:shared_preferences_settings/shared_preferences_settings.dart';

import '../constants.dart';

class SettingsUtils {
  static final SettingsUtils _instance = SettingsUtils._internal();
  Settings _settings = Settings();

  factory SettingsUtils() {
    return _instance;
  }

  SettingsUtils._internal();

  void setIsSetup() {
    _settings.save(kPrefIsSetup, true);
  }

  Future<bool> isSetup() async {
    return _settings.getBool(kPrefIsSetup, false);
  }

  void setIntroShown() {
    _settings.save(kPrefIntroShown, true);
  }

  Future<bool> isIntroShown() async {
    return _settings.getBool(kPrefIntroShown, false);
  }

  void setImageFile(String path) {
    _settings.save(kPrefKeyImageFile, path);
  }

  Future<String> getImageFile() async {
    return _settings.getString(kPrefKeyImageFile, '');
  }

  // Used for testing
  void setSettings(Settings instance) {
    _settings = instance;
  }

  Settings getSettings() {
    if (_settings == null) {
      _settings = Settings();
    }

    return _settings;
  }

  Future<String> getString(String key, String defaultValue) async {
    return getSettings().getString(key, defaultValue);
  }

  Future<ContactCard> getMyInfo() async {
    String name = await getString(kPrefKeyName, '');
    String title = await getString(kPrefKeyTitle, '');
    String phone = await getString(kPrefKeyPhone, '');
    String email = await getString(kPrefKeyEmail, '');

    ContactCard info = ContactCard(name, title, phone, email);
    return info;
  }
}
