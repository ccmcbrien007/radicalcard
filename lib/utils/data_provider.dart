import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'package:radicalcard/model/contact_card.dart';
import 'package:sqflite/sqflite.dart';

import '../constants.dart';

class DataProvider {
  DataProvider._();

  static final DataProvider provider = DataProvider._();

  Database _database;

  Future<Database> get database async {
    if (_database == null) {
      _database = await getDatabaseInstance();
      //await _insertTestData();
    }
    return _database;
  }

  Future<Database> getDatabaseInstance() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, kDatabaseName);
    //if (await Directory(dirname(path)).exists()) {
    //await deleteDatabase(path);
    //}
    return await openDatabase(
      path,
      version: 4,
      onUpgrade: (db, oldVersion, newVersion) async {
        print('Upgrading database');
        await db.execute(
            'ALTER TABLE $kTableNameContactCard ADD $kColumnNameContactCardLat DOUBLE');
        await db.execute(
            'ALTER TABLE $kTableNameContactCard ADD $kColumnNameContactCardLon DOUBLE');
      },
      onCreate: (Database db, int version) async {
        await db.execute('CREATE TABLE $kTableNameContactCard ('
            '$kColumnNameId integer primary key AUTOINCREMENT,'
            '$kColumnNameName TEXT,'
            //Todo exctract constants
            'title TEXT,'
            'phone TEXT,'
            'email TEXT,'
            'notes TEXT,'
            '$kColumnNameContactCardLat DOUBLE,'
            '$kColumnNameContactCardLon DOUBLE'
            ')');
      },
    );
  }

  //return all persons from the database
  Future<List<ContactCard>> getAllCards() async {
    final db = await database;
    var response =
        await db.query(kTableNameContactCard, orderBy: kColumnNameName);
    List<ContactCard> list =
        response.map((c) => ContactCard.fromMap(c)).toList();
    return list;
  }

//return single person with id
  Future<ContactCard> getPersonWithId(int id) async {
    final db = await database;
    var response = await db.query(kTableNameContactCard,
        where: '$kColumnNameId = ?', whereArgs: [id]);
    return response.isNotEmpty ? ContactCard.fromMap(response.first) : null;
  }

  Future<int> addCardToDatabase(ContactCard card) async {
    final db = await database;
    var raw = await db.insert(
      kTableNameContactCard,
      card.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return raw;
  }

  //delete person with id
  deleteCardWithId(int id) async {
    final db = await database;
    return db.delete(kTableNameContactCard,
        where: '$kColumnNameId = ?', whereArgs: [id]);
  }

//delete all persons
  deleteAllCards() async {
    final db = await database;
    db.delete(kTableNameContactCard);
  }

  updatePerson(ContactCard person) async {
    final db = await database;
    var response = await db.update(kTableNameContactCard, person.toMap(),
        where: '$kColumnNameId = ?', whereArgs: [person.id]);
    return response;
  }

  Future<int> getRowCount() async {
    final db = await database;
    int count = Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM $kTableNameContactCard'));
    return count;
  }

  // Future<void> _insertTestData() async {
  //   await deleteAllCards();
  //
  //   final List<ContactCard> testData = [
  //     ContactCard('Albert Einstein', 'Physicist', '212-000-0000',
  //         'einstein@princeton.edu'),
  //     ContactCard('Chris McBrien', 'Code Slinger', '403-555-1111',
  //         'jjack.flash42@gmail.com'),
  //     ContactCard('Joe Biden', 'POTUS', '214-650-0000', 'potus@whitehouse.gov'),
  //     ContactCard(
  //         'Amos Burton', 'Mechanic', '011-41-9154576', 'amos@rocinante.un'),
  //     ContactCard('Linus Pauling', 'Scientist', '212-111-1111',
  //         'lpauling@cambridge.edu'),
  //     ContactCard('Leonardo Davinci', 'Artist', 'none', 'nada'),
  //     ContactCard('Linus Torvalds', 'Kernel Developer', '011-23-451-35671',
  //         'linus.torvalds@linux.org'),
  //     ContactCard(
  //         'James Holden', 'Captain', '011-41-9154574', 'james@rocinante.un'),
  //     ContactCard('John Oliver', 'Comedian', '212-213-5977', 'joliver@hbo.com'),
  //   ];
  //
  //   testData.forEach((element) async {
  //     await addCardToDatabase(element);
  //   });
  // }
}
