import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class DialogUtility {
  Future<void> showMyDialog(BuildContext context, String title, String message,
      List<String> buttons, ValueChanged<String> buttonPressHandler) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(title),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text(message),
              ],
            ),
          ),
          actions: _getButtons(context, buttons, buttonPressHandler),
        );
      },
    );
  }

  List<Widget> _getButtons(BuildContext context, List<String> buttons,
      ValueChanged<String> buttonPressHandler) {
    List<Widget> result = List<Widget>();
    buttons.forEach((element) {
      result.add(FlatButton(
        child: Text(element),
        onPressed: () {
          buttonPressHandler(element);
          Navigator.of(context).pop();
        },
      ));
    });
    return result;
  }
}
