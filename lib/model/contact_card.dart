import 'package:quiver/core.dart';

class ContactCard {
  int id;
  String name;
  String title;
  String email;
  String phone;
  String notes;

  /// Latitude in degrees
  double latitude;

  /// Longitude, in degrees
  double longitude;

  ContactCard(this.name, this.title, this.phone, this.email,
      [this.notes, this.latitude, this.longitude, this.id]);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ContactCard &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          title == other.title &&
          phone == other.phone &&
          email == other.email;

  @override
  int get hashCode => hashObjects([name, title, phone, email]);

  Map<String, dynamic> toMap() => {
        "id": id,
        "name": name,
        "title": title,
        "phone": phone,
        "email": email,
        "notes": notes,
        "latitude": latitude,
        "longitude": longitude,
      };

  factory ContactCard.fromMap(Map<String, dynamic> json) => ContactCard(
        json["name"],
        json["title"],
        json["phone"],
        json["email"],
        json["notes"],
        json["latitude"],
        json["longitude"],
        json["id"],
      );

  bool hasLocation() {
    return this.longitude != null && this.latitude != null;
  }
}
