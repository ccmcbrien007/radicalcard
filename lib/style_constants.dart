import 'package:flutter/material.dart';

const kNameTextStyle = TextStyle(
    fontFamily: 'MuseoModerno',
    fontSize: 40.0,
    color: Colors.white,
    fontWeight: FontWeight.bold);
const kTitleTextStyle = TextStyle(
    fontFamily: 'SourceSansPro',
    fontSize: 20.0,
    color: Colors.tealAccent,
    letterSpacing: 2.5);
const kBoxTextStyle =
    TextStyle(color: Colors.teal, fontFamily: 'SourceSansPro', fontSize: 20.0);
const kTextStyleAppBarAction = TextStyle(color: Colors.lightBlue, fontSize: 20);
const kTextStyleEditFieldLabel =
    TextStyle(fontFamily: 'SourceSansPro', fontSize: 20);
