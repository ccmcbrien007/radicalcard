# Radical Card

A contact card app by **Radical Speed** for Android and available on the [Google Play Store](https://play.google.com/store/apps/details?id=ca.radicalspeed.radicalcard).  
This application is presented as part of the [skills portfolio](https://radicalspeed.ca/#/) for **Chris McBrien** and was created with [Flutter](https://flutter.dev/).



